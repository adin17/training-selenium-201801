# Test Automation dengan Selenium #

> transformasi dari click tester menjadi test engineer

Materi Training

* Pengantar Automated Testing
* Merekam functional test dengan Selenium IDE
* Membuat Automated Test dengan JUnit
* Membuat test program dengan Selenium Web Driver
* PageObject pattern untuk merapikan kumpulan test
* Paralel test dengan Selenium Grid
* [Continuous Integration dengan Gitlab-CI](https://gitlab.com/endymuhardin/belajar-selenium-ci)

## Kebutuhan Software ##

Browser:

* Chrome
* Firefox

Browser Add-Ons:

* [Selenium IDE](https://www.seleniumhq.org/projects/ide/)

Browser Driver:

* Chrome Driver
* Gecko Driver

Test Automation:

* Java SDK 8
* Apache Maven 3
* IDE

    * Netbeans
    * Eclipse
    * VS Code

Continuous Integration

* Gitlab-CI
* Jenkins

## Setup Database ##

1. Create database user

        grant all on belajar.* to belajar@localhost identified by 'java';

2. Create database

        create database belajar

## Selenium Grid ##

![Diagram Selenium Grid](selenium-grid.jpg)

## Referensi ##

* http://www.codeyouneed.com/parameterized-junit-test/
* https://github.com/Pragmatists/junitparams-spring-integration-example
* https://medium.com/@cabot_solutions/a-guide-to-using-selenium-for-continuous-integration-testing-c2b43f4d4872
* https://gitlab.com/gitlab-org/gitlab-selenium-server
* https://about.gitlab.com/2017/12/19/moving-to-headless-chrome/
* http://elementalselenium.com/tips

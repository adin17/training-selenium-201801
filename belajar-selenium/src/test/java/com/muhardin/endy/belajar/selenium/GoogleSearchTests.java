package com.muhardin.endy.belajar.selenium;

import java.io.File;
import java.util.UUID;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleSearchTests {
    
    private static WebDriver driver;
    
    @BeforeClass
    public static void bukaBrowser(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }
    
    @AfterClass
    public static void tutupBrowser(){
        driver.quit();
    }

    @Test
    public void testCariKata() throws Exception {
        String kata = "belajar selenium";
        System.out.println("Mencari kata " +kata);

        String website = "https://google.com";
        
        // buka website google
        driver.get(website);
        
        // cari input field yang namanya q
        WebElement inputCari = driver.findElement(By.name("q"));
        inputCari.sendKeys(kata);
        inputCari.submit();
        
        // tunggu 5 detik sampai hasil search keluar
        Boolean sukses = (new WebDriverWait(driver, 5))
                .until(ExpectedConditions.titleContains(kata));
        
        Assert.assertTrue("Hasil search didapatkan", sukses);
        
        String title = driver.getTitle();
        Assert.assertNotNull(title);
        System.out.println("Title setelah search : "+title);
        
        // ambil screenshot
        Thread.sleep(3000);
        String fileScreenshot = System.getProperty("java.io.tmpdir") 
                + UUID.randomUUID().toString() + ".png";
        System.out.println("Menyimpan screenshot ke : "+fileScreenshot);
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        scrFile.renameTo(new File(fileScreenshot));
    }
}
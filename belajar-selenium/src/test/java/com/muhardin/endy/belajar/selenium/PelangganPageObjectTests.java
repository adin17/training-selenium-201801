package com.muhardin.endy.belajar.selenium;

import com.github.javafaker.Faker;
import com.muhardin.endy.belajar.selenium.helper.SeleniumUtilities;
import com.muhardin.endy.belajar.selenium.pageobject.FormEditPelanggan;
import com.muhardin.endy.belajar.selenium.pageobject.JenisKelamin;
import com.muhardin.endy.belajar.selenium.pageobject.PendidikanTerakhir;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PelangganPageObjectTests {
    private static final String URL_APLIKASI = "http://training-selenium.herokuapp.com/pelanggan";
    private static final String PESAN_SUKSES = "Data pelanggan berhasil disimpan";
    
    private static WebDriver webDriver;
    private Faker faker = new Faker();
    
    @BeforeClass
    public static void jalankanBrowser(){
        webDriver = new FirefoxDriver();
        webDriver.manage().window().maximize();
    }
    
    @AfterClass
    public static void tutupBrowser(){
        webDriver.quit();
    }
    
    @Test
    public void testInputPelanggan(){
        webDriver.get(URL_APLIKASI+"/form");
        
        // membuat page object untuk form input pelanggan
        FormEditPelanggan form = new FormEditPelanggan(webDriver);
        
        // isi form
        form.isiNama("Pelanggan 002");
        form.isiEmail("p002@hotmail.com");
        form.isiTanggalLahir("1945-08-17");
        form.pilihJenisKelamin(JenisKelamin.WANITA);
        form.pilihPendidikanTerakhir(PendidikanTerakhir.D4);
        form.simpan();
        
        // periksa hasilnya
        Boolean pindahKeHalamanList = (new WebDriverWait(webDriver, 5))
                .until(ExpectedConditions.urlContains("pelanggan/list"));
        
        Assert.assertTrue(pindahKeHalamanList);
        Assert.assertTrue(webDriver.findElement(By.tagName("body"))
                .getText().contains(PESAN_SUKSES));
        SeleniumUtilities.ambilScreenshot(webDriver, "simpan-normal");
    }
}
